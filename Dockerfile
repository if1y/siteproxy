FROM node:12.22.7-alpine3.11
MAINTAINER if1y

RUN apk update \
 && apk add --no-cache git \
 && git clone --progress https://github.com/netptop/siteproxy.git /siteproxy \
 && cd /siteproxy \
 && npm i
 
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

ENV SERVER_HOST siteproxy.netptop.workers.dev
ENV SERVER_PORT 8011

ENTRYPOINT /entrypoint.sh