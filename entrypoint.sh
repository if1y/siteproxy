#!/bin/sh

echo "==================================="
echo "          Setting Config           "
echo "==================================="
sed -i "s/siteproxy.netptop.workers.dev/$SERVER_HOST/g" /siteproxy/config.js
sed -i "s/8011/$SERVER_PORT/g" /siteproxy/config.js
sed -i "s/8011/$SERVER_PORT/g" /siteproxy/index.js

echo "==================================="
echo "     Starting with $SERVER_HOST    "
echo "==================================="
node /siteproxy/index.js --tls-min-v1.0

echo "==================================="
echo " Listening at 0.0.0.0:$SERVER_PORT "
echo "==================================="